# Desciption

1/10/2017

The tech review comments were based on my experiences at the time of writing this.
State management without a router was experimental so it's definitely not perfectly implemented.

Experimentation project with a new front-end stack and build process. The project is also responsive so please resize it. The mobile version is messed up now on initial size and doesn't fit to screen properly but I have no more time to look into it at the moment so if you can tell me why please do.  

- Build: Rollup
- Stack: Inferno, Redux, nodejs
- Browser: Chrome
- CSS: flex box

## Thoughts

### Rollup:

Rollup is definitely cool and would recommend it if you looking for fast build processes and have a large project that you need to save some bytes.

#### Cons

It's definitely new so it's lacking some plugin functionality depending how you want to manage your project.  

My biggest complaint is managing CSS (maybe I missed something to hook up correctly but I wasted too much time trying). It does not resolve SASS 'import' statements! The only way to manage CSS and recompile during Rollup's watch is to require CSS in your .js modules. However, it will only watching the files you include so you will need to import a style sheet for each module which is probably a good management practice to make sure you're getting the correct CSS per component. This sounds cool until you realize there's no way to do SASS global variables because of how the SASS plugin is processing the modules. This is a huge pain and takes away a main feature of SASS. Setting up and using livereload seems like a pain too.

In my opinion, I would go with Webpack 2. It's the most solid front-end build tool with the most functionality for optimizations at the moment that has been tested.

### Inferno:

The inferno library itself is great. If you're going to build a React app bring yourself up with a leaner and faster library like Inferno since there's really no learning curve. 

#### Cons

No real cons I can see with inferno but my problems come from the architecture solutions that were ported over for inferno for routing and state management. Found some bugs with most of the copy cat libraries and opened issues.

My conclusion is to use inferno but be cautious about third-party stuff as usual. 

# Installation

- Download and install nodejs: https://nodejs.org/en/
- From project root run: npm install

# Production

- From project root run: npm run start
- Visit localhost:3000 in a browser.

# Development:

- Open fresh terminal -> from project root run: npm run start-server-dev
- Open fresh terminal -> from project root run: npm run start-client-dev
- All changes made to .js and .scss files will cause a recompile.
- .scss files will only be watch if they're being imported by a .js file within the project. There is no need to use sass import statements to compile the css using Rollup.
- Visit localhost:3000 in a browser.