'use strict';

import '../../styles/components/_select-dropdown.scss';
import Inferno from 'inferno';
const createVNode = Inferno.createVNode;
import InfernoComponent from 'inferno-component';

/**
@class SelectDropdown
@constructor
@param props {object} -
	children {Array}
	title {String}
	selection {String|Number}
	items {Array}
	handleToggleList {Function}
	showList {boolean}
*/
export default class SelectDropdown extends InfernoComponent {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div className={this.props.className + ' select-dropdown'}>
				<label className="select-dropdown_title">
					{this.props.title}
				</label>
				<div
					className="select-dropdown_selection"
					onClick={this.props.handleToggleList}
				>
					{this.props.selection}
				</div>
				{
					this.props.showList &&
					<ol className="select-dropdown_list">
						{
							this.props.items && this.props.items.map((item) => {
								return (
									<li
										className="select-dropdown-list_item"
										value={item.value}
										onClick={this.props.handleSelectionChange}
									>
										<label className="select-dropdown-list-item_contents">
											{item.display}
										</label>
									</li>
								);
							})
						}
					</ol>
				}
			</div>
		);
	}
}
