import '../../styles/components/_campaign-card.scss';

'use strict';

import Inferno from 'inferno';
const createVNode = Inferno.createVNode;
import InfernoComponent from 'inferno-component';

/**
@class CampaignCard
@constructor
@param props {object} - {
	children {Array}
	campaignId {String}
	title {String}
	description {String}
	coveragePercentage {String}
	handleDeleteCampaign {Function}
	handleDuplicateCampaign {Function}
	showActionsHandler {Function}
	showActions {boolean}
}
*/
export default class CampaignCard extends InfernoComponent {
	constructor(props) {
		super(props);
	}

	render() {
		return (
			<div className="campaign-card">
				<div className="campaign-card_display">
					<p className="campaign-card_header">
							{this.props.title}
					</p>
					<p className="campaign-card_body">
						{this.props.description}
					</p>
				</div>
				<div className="campaign-card_footer">
					<div className="campaign-card-footer_item">
						{this.props.conversionPercentage + '%'}
					</div>
					<div className="campaign-card-footer_item">
						{this.props.campaignId}
					</div>
					<input
						className="campaign-card-footer_item"
						type="button"
						value="..."
						onClick={this.props.showActionsHandler}
						data-campaign-id={this.props.campaignId}
					/>
				</div>
				{
					this.props.showActions &&
					<CampaignCardActions
						campaignId={this.props.campaignId}
						handleDuplicateCampaign={this.props.handleDuplicateCampaign}
						handleDeleteCampaign={this.props.handleDeleteCampaign}
						handleToggleActions={this.props.handleToggleActions}
					/>
				}
			</div>
		);
	}
}

function CampaignCardActions(props) {
	return (
		<div
			className="campaign-card_actions"
			onClick={props.handleToggleActions}
		>
			<a
				className="campaign-card-actions_link"
				href={`/#/campaigns/${props.campaignId}/rules`}
			>
				RULES
			</a>
			<a
				className="campaign-card-actions_link"
				href={`/#/campaigns/${props.campaignId}/edit`}
			>
				EDIT
			</a>
			<a
				className="campaign-card-actions_link"
				href="#"
				onclick={props.handleDuplicateCampaign}
				data-campaign-id={props.campaignId}
			>
				DULICATE
			</a>
			<a
				className="campaign-card-actions_link"
				href="#"
				onclick={props.handleDeleteCampaign}
				data-campaign-id={props.campaignId}
			>
				DELETE
			</a>
		</div>
	);
}
