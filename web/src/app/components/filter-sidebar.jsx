'use strict';

import '../../styles/components/_filter-sidebar.scss';
import Inferno from 'inferno';
const createVNode = Inferno.createVNode;
import InfernoComponent from 'inferno-component';
import SelectDropdown from './select-dropdown.jsx';

const linkEvent = Inferno.linkEvent;

/**
@class FilterSidebar
@constructor
@param props {object} -
	children : 'array'
	filterBy : 'string'
	sortBy : 'string'
	filterByOptions : 'array'
	sortByOptions : 'array'
	handleFiltersChange {Function}
*/
export default class FilterSidebar extends InfernoComponent {
	constructor(props) {
		super(props);

		this.state = {
			sortBySelection: props.sortBy || '',
			filterBySelection: props.filterBy || '',
			searchQuery: props.searchQuery || '',
			showSortByOptions: false,
			showFilterByOptions: false,
			menuHidden: false
		};
	}

	render() {
		return (
			<div
				className={'filter-sidebar ' +
						(this.state.menuHidden
						? 'filter-sidebar_menu--toggle'
						: '')}
			>
				<div
					className="filter-sidebar_menu-button"
					onClick={linkEvent(this, handleToggleMenu)}
				>
				</div>
				<div className="filter-sidebar_menu">
					<label className="filter-sidebar_header">
						Filters
					</label>
					<input
						className="filter-sidebar_button"
						type="button"
						value="Reset Filters"
						onMouseDown={linkEvent(this, handleResetFilters)}
					/>
					<input
						className="filter-sidebar_search"
						type="text"
						autofocus
						placeholder="Search"
						value={this.state.searchQuery}
						onInput={linkEvent(this, handleQueryChange)}
					/>
					<div className="filter-sidebar_section">
						<SelectDropdown
							title="Sort By"
							items={this.props.sortByOptions}
							selection={this.state.sortBySelection}
							handleSelectionChange={linkEvent(this, handleSortSelectionChange)}
							handleToggleList={linkEvent(this, handleToggleSortDropdown)}
							showList={this.state.showSortByOptions}
						/>
					</div>
					<div className="filter-sidebar_section">
						<SelectDropdown
							title="Filter By"
							items={this.props.filterByOptions}
							selection={this.state.filterBySelection}
							handleSelectionChange={linkEvent(this, handleFilterSelectionChange)}
							handleToggleList={linkEvent(this, handleToggleFilterDropdown)}
							showList={this.state.showFilterByOptions}
						/>
					</div>
				</div>
			</div>
		);
	}
}

function handleToggleMenu(props, event){
	console.log(props.state.menuHidden)
	props.setState({
		menuHidden: !props.state.menuHidden
	});
}

function handleToggleSortDropdown(props, event) {
	props.setState({
		showSortByOptions: !props.state.showSortByOptions,
		showFilterByOptions: false
	});
}

function handleToggleFilterDropdown(props, event) {
	props.setState({
		showFilterByOptions: !props.state.showFilterByOptions,
		showSortByOptions: false
	});
}

function handleResetFilters(props, event) {
	props.setState({
		selectedDropdown: '',
		showSortByOptions: false,
		searchQuery: '',
		filterBySelection: props.props.filterByOptions[0].value,
		sortBySelection: props.props.sortByOptions[0].value
	}, () => {
		props.props.handleFiltersChange(
			props.state.filterBySelection,
			props.state.sortBySelection,
			props.state.searchQuery
		);
	});
}

function handleQueryChange(props, event) {
	props.setState({
		searchQuery: event.target.value
	}, () => {
		props.props.handleFiltersChange(
			props.state.filterBySelection,
			props.state.sortBySelection,
			props.state.searchQuery
		);
	});
}

function handleSortSelectionChange(props, event) {
	props.setState({
		sortBySelection: event.target.textContent,
		showSortByOptions: false
	}, () => {
		props.props.handleFiltersChange(
			props.state.filterBySelection,
			props.state.sortBySelection,
			props.state.searchQuery
		);
	});
}

function handleFilterSelectionChange(props, event) {
	props.setState({
		filterBySelection: event.target.textContent,
		showFilterByOptions: false
	}, () => {
		props.props.handleFiltersChange(
			props.state.filterBySelection,
			props.state.sortBySelection,
			props.state.searchQuery
		);
	});
}
