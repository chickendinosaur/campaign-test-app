import { createStore } from 'redux';
import campaignsReducer from './reducers/campaigns';

export default createStore(campaignsReducer, {
	currentFilterOptions: {
		filterBy: 'name',
		sortBy: 'ascending'
	}
});
