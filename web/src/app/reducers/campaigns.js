'use strict';

import fetchCampaignsAC from '../actions/fetch-campaigns';

export default function (state, action) {
	switch (action.type) {
	case 'fetchCampaigns':
		var s = {
			...state,
			campaigns: {
				campaignItems: null,
				notification: null
			},
			currentFilterOptions: {
				filterBy: action.filterBy,
				sortBy: action.sortBy,
				searchQuery: action.searchQuery
			}
		};

		if (action.status === 'pending') {
			s.campaigns.notification = 'Retrieving campaigns.';
		} else if (action.status === 'error') {
			s.campaigns.notification = 'Unable to retrieve campaigns.';
		} else if (action.status === 'success') {
			s.campaigns.campaignItems = action.payload.data.campaigns;
		}

		return s;
	// case 'editCampaign':
	// 	var s = {
	// 		...state
	// 	};

	// 	return s;
	case 'duplicateCampaign':
		// Note:
		// Calling another action should probably be done inside
		// an action creator since no state is updated.
		if (action.status === 'success') {
			fetchCampaignsAC(
				state.currentFilterOptions.filterBy,
				state.currentFilterOptions.sortBy
			);
		}

		return state;
	case 'deleteCampaign':
		if (action.status === 'success') {
			fetchCampaignsAC(
				state.currentFilterOptions.filterBy,
				state.currentFilterOptions.sortBy
			);
		}

		return state;
	case 'fetchCampaignFilters':
		var s = {
			...state,
			filters: {
				filterBy: action.payload.filter.filterTypes,
				sortBy: action.payload.sort.sortTypes
			}
		};

		return s;
	}

	return state;
}
