import '../styles/index.scss';
import Inferno from 'inferno';
const createVNode = Inferno.createVNode;
import App from './containers/app.jsx';

Inferno.render(
	<App />,
	document.getElementById('app')
);
