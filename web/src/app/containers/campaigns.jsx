'use strict';

import '../../styles/components/_campaigns.scss';
import Inferno from 'inferno';
const createVNode = Inferno.createVNode;
import InfernoComponent from 'inferno-component';
import CampaignCard from '../components/campaign-card.jsx';
import deleteCampaignAC from '../actions/delete-campaign';
import duplicateCampaignAC from '../actions/duplicate-campaign';

const linkEvent = Inferno.linkEvent;

/**
@class Campaigns
@constructor
@param props {Object} -
	children {Array}
	campaigns {Array|null|undefined}
	filters {Object|null|undefined}
*/
export default class Campaigns extends InfernoComponent {
	constructor(props) {
		super(props);

		this.state = {
			showCampaignActionsId: null
		};
	}

	render() {
		return (
			<div className="campaigns">
				{
					this.props.campaigns && this.props.campaigns.map((item) => {
						return (
							<CampaignCard
								key={item.id}
								campaignId={item.id}
								title={item.title}
								description={item.description}
								conversionPercentage={item.conversionPercentage}
								showActionsHandler={linkEvent(this, handleShowCampaignActions)}
								showActions={this.state.showActions && this.state.showCampaignActionsId === item.id}
								handleDeleteCampaign={linkEvent(this, handleDeleteCampaign)}
								handleDuplicateCampaign={linkEvent(this, handleDuplicateCampaign)}
								handleToggleActions={linkEvent(this, handleToggleActions)}
							/>
						);
					})
				}
			</div>
		);
	}
}

function handleShowCampaignActions(props, event) {
	props.setState({
		showCampaignActionsId: parseInt(event.target.dataset.campaignId),
		showActions: true
	});
}

function handleDeleteCampaign(props, event) {
	event.preventDefault();
	deleteCampaignAC(event.target.dataset.campaignId);

	props.setState({
		showCampaignActionsId: parseInt(event.target.dataset.campaignId),
		showActions: false
	});
}

function handleDuplicateCampaign(props, event) {
	event.preventDefault();
	duplicateCampaignAC(event.target.dataset.campaignId);

	props.setState({
		showCampaignActionsId: parseInt(event.target.dataset.campaignId),
		showActions: false
	});
}

function handleToggleActions(props, event) {
	props.setState({
		showCampaignActionsId: parseInt(event.target.dataset.campaignId),
		showActions: false
	});
}
