'use strict';

import Inferno from 'inferno';
const createVNode = Inferno.createVNode;
import InfernoComponent from 'inferno-component';
import getObjectProperty from '@chickendinosaur/object-get-property';
import store from '../store';
import Campaigns from './campaigns.jsx';
import FilterSidebar from '../components/filter-sidebar.jsx';
import fetchCampaignFiltersAC from '../actions/fetch-campaign-filters';
import fetchCampaignsAC from '../actions/fetch-campaigns';

/**
@class App
@constructor
@param props {object} - {
	children : 'array'
}
*/
export default class App extends InfernoComponent {
	constructor(props) {
		super(props);

		this.state = store.getState();

		store.subscribe(() => {
			this.setState(store.getState());
		});

	}

	componentWillMount() {
		fetchCampaignFiltersAC();
		fetchCampaignsAC(
			store.getState().currentFilterOptions.filterBy,
			store.getState().currentFilterOptions.sortBy
		);
	}

	render() {
		return (
			<div className="app">
				<Campaigns
					campaigns={getObjectProperty(this.state.campaigns, 'campaignItems')}
				/>
				{
					this.state.filters &&
					<FilterSidebar
						filterBy={this.state.currentFilterOptions.filterBy}
						sortBy={this.state.currentFilterOptions.sortBy}
						filterByOptions={this.state.filters.filterBy}
						sortByOptions={this.state.filters.sortBy}
						handleFiltersChange={handleCampaignFilters}
					/>
				}
			</div>
		);
	}
}

function handleCampaignFilters(filterBy, sortBy, searchQuery) {
	fetchCampaignsAC(
		filterBy,
		sortBy,
		searchQuery
	);
}
