'use strict';

import store from '../store';

export default function deleteCampaign(campaignId) {
	fetch(`/api/v1/campaigns/${campaignId}`, {
			method: 'DELETE'
		})
		.then(function (res) {
			return res.json();
		})
		.then(function (data) {
			store.dispatch({
				type: 'deleteCampaign',
				payload: data,
				status: 'success'
			});
		})
		.catch(function (err) {
			store.dispatch({
				type: 'deleteCampaign',
				status: 'error'
			});
		});
}
