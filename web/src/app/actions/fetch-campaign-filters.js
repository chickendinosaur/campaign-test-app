'use strict';

import store from '../store';

export default function fetchCampaignFilters() {
	var filterByPromise = fetch(`/api/v1/campaigns/query-param-types/filter`)
		.then(function (res) {
			return res.json();
		});

	var sortByPromise = fetch(`/api/v1/campaigns/query-param-types/sort`)
		.then(function (res) {
			return res.json();
		});

	Promise.all([filterByPromise, sortByPromise])
		.then(function (values) {
			store.dispatch({
				type: 'fetchCampaignFilters',
				payload: {
					filter: values[0],
					sort: values[1]
				},
				status: 'success'
			});
		})
		.catch(function (res) {
			store.dispatch({
				type: 'fetchCampaignFilters',
				status: 'error'
			});
		});
}
