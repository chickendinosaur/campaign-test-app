'use strict';

import store from '../store';

export default function duplicateCampaign(campaignId) {
	fetch(`/api/v1/campaigns/${campaignId}`, {
			method: 'POST'
		})
		.then(function (res) {
			return res.json();
		})
		.then(function (data) {
			store.dispatch({
				type: 'duplicateCampaign',
				payload: data,
				status: 'success'
			});
		})
		.catch(function (err) {
			store.dispatch({
				type: 'duplicateCampaign',
				status: 'error'
			});
		});
}
