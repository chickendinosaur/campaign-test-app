'use strict';

import querystring from 'querystring';
import store from '../store';

export default function fetchCampaigns(filterBy, sortBy, searchQuery) {
	var query = {
		filter: filterBy,
		sort: sortBy,
		q: searchQuery
	};

	var qs = querystring.stringify(query);

	if (qs.length > 0) {
		qs = '?' + qs;
	}

	fetch(`/api/v1/campaigns${qs}`)
		.then(function (res) {
			return res.json();
		})
		.then(function (data) {
			store.dispatch({
				type: 'fetchCampaigns',
				payload: data,
				filterBy: filterBy,
				sortBy: sortBy,
				searchQuery: searchQuery,
				status: 'success'
			});
		})
		.catch(function (err) {
			store.dispatch({
				type: 'fetchCampaigns',
				status: 'error'
			});
		});
}
