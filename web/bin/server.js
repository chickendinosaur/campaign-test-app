'use strict';

const path = require('path');
const express = require('express');
const compression = require('compression');
const bodyParser = require('body-parser');

const app = express();
const router = express.Router();

// Set environment variables.
process.env.PORT = process.env.PORT || 3000;
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

var dirToUse = process.env.NODE_ENV === 'production' ? 'public' : 'build';

// Middleware.
router.use(bodyParser.json());
router.use(bodyParser.urlencoded({ extended: true }));
router.use(compression());
router.use(express.static(path.join(__dirname, '..', dirToUse)));

var apiRootPath = '../../api';
require(path.join(apiRootPath, 'master'))(router);

// Serve site.
router.get('/', function (req, res) {
	res.sendFile(path.join(__dirname, '..', dirToUse), 'index.html');
});

app.use('/', router);

// Start the server.
app.listen(process.env.PORT, function () {
	// Log server information.
	console.log('Server listening on port: ' + process.env.PORT);
});
