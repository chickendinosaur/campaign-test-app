// Rollup plugins
import babel from 'rollup-plugin-babel';
import commonjs from 'rollup-plugin-commonjs';
import resolve from 'rollup-plugin-node-resolve';
import replace from 'rollup-plugin-replace';
import sass from 'rollup-plugin-sass';
import builtins from 'rollup-plugin-node-builtins';

var isProduction = process.env.NODE_ENV === 'production';

export default {
	entry: 'src/client-entry.js',
	dest: 'build/index.js',
	format: 'iife',
	sourceMap: false,
	plugins: [
		builtins(),
		resolve({
			module: true, // Default: true
			jsnext: true, // Default: false
			main: true, // Default: true
			skip: [], // Default: []
			browser: true, // Default: false
			extensions: ['.js', '.json', '.jsx'], // Default: ['.js']
			preferBuiltins: true // Default: true

		}),
		sass({
			output: 'build/index.css',
			includePaths: ['src/styles']
		}),
		commonjs(),
		babel({
			exclude: 'node_modules/**'
		}),
		replace({
			'process.env.NODE_ENV': isProduction ? JSON.stringify('production') : JSON.stringify('development')
		})
	]
};
