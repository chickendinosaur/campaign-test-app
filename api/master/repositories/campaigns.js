'use strict';

const path = require('path');
const fs = require('fs');

module.exports = {
	getCampaign,
	getCampaigns,
	updateCampaign,
	createCampaign,
	deleteCampaign,
	getQueryParamTypes
};

function _filterCampaignsBy(campaigns, field) {
	// Sort keys.
	var sorterKeys = [];
	var numCampaigns = campaigns.length;
	for (let i = 0; i < numCampaigns; i++) {
		sorterKeys.push(campaigns[i][field]);
	}
	sorterKeys.sort();

	// Create sorted campaign list.
	var sortedCampaignList = [];
	var campaignName;
	for (let i = 0; i < numCampaigns; i++) {
		campaignName = sorterKeys[i];

		for (let j = 0; j < numCampaigns; j++) {
			if (campaignName === campaigns[j][field]) {
				sortedCampaignList.push(campaigns[j]);
				break;
			}
		}
	}

	return sortedCampaignList;
}

function getCampaigns(query) {
	return new Promise(function (resolve, reject) {
		var result = null;
		fs.readFile(
			path.join(__dirname, '..', 'data', 'campaigns.json'),
			function (err, data) {
				data = JSON.parse(data);
				var campaigns = data.campaigns;
				result = campaigns;

				if (query !== undefined) {
					// Search by
					var searchQuery = query.q;
					if (searchQuery !== undefined) {
						searchQuery = query.q.toLowerCase();
						for (let i = 0; i < campaigns.length; i++) {
							var title = campaigns[i].title.toString().toLowerCase();
							var description = campaigns[i].description.toLowerCase();
							if (title.constructor !== String) {
								title = title.toString();
							}

							if (description.constructor !== String) {
								description = description.toString();
							}

							if (description.indexOf(searchQuery) === -1 &&
								title.indexOf(searchQuery) === -1) {
								campaigns.splice(i, 1);
								i--;
							}
						}
					}

					// Filter
					var filter = query.filter;
					if (filter !== undefined) {
						if (filter === 'name') {
							result = _filterCampaignsBy(campaigns, 'title');
						} else if (filter === 'created') {
							result = _filterCampaignsBy(campaigns, 'createdTimestamp');
						} else if (filter === 'updated') {
							result = _filterCampaignsBy(campaigns, 'updatedTimestamp');
						}
					}

					// Sort
					var sort = query.sort;
					if (sort !== undefined) {
						if (sort === 'descending') {
							result.reverse();
						}
					}
				}

				resolve(result);
			});
	});
}

function createCampaign(params, query) {
	var result = null;
	var data = JSON.parse(fs.readFileSync(path.join(__dirname, '..', 'data', 'campaigns.json')));
	var campaigns = data.campaigns;
	var campaginId = parseInt(params.id);

	// Grab the campaign to duplicate.
	var campaign;
	for (var i = 0; i < campaigns.length; i++) {
		if (campaigns[i].id === campaginId) {
			campaign = campaigns[i];
			break;
		}
	}

	// Duplicate the campaign.
	if (campaign) {
		var campaignCopy = Object.assign({}, campaign);
		campaignCopy.title = '(copy)' + campaignCopy.title;
		campaignCopy.updatedTimestamp = new Date().toISOString();
		campaignCopy.createdTimestamp = new Date().toISOString();
		campaigns.push(campaignCopy);
		++data.idCounter;
		campaignCopy.id = data.idCounter;
		// Save campaigns to disk.
		fs.writeFileSync(path.join(__dirname, '..', 'data', 'campaigns.json'), JSON.stringify(data));
		result = campaignCopy;
	}

	return result;
}

function deleteCampaign(params, query) {
	var result = null;
	var data = JSON.parse(fs.readFileSync(path.join(__dirname, '..', 'data', 'campaigns.json')));
	var campaigns = data.campaigns;
	var campaginId = parseInt(params.id);

	for (var i = 0; i < campaigns.length; i++) {
		if (campaigns[i].id === campaginId) {
			result = campaigns.splice(i, 1);
			break;
		}
	}

	// Save campaigns to disk.
	fs.writeFileSync(path.join(__dirname, '..', 'data', 'campaigns.json'), JSON.stringify(data));

	return result;
}

function updateCampaign(params, body) {
	var result = null;
	var data = JSON.parse(fs.readFileSync(path.join(__dirname, '..', 'data', 'campaigns.json')));
	var campaigns = data.campaigns;
	var campaginId = parseInt(params.id);
	var campaign;
	var updatedCampaign = body;

	for (var i = 0; i < campaigns.length; i++) {
		campaign = campaigns[i];
		if (campaign.id === campaginId) {
			// Set campaign to update contents to passed in contents.
			if (updatedCampaign.title !== undefined) {
				campaign.title = updatedCampaign.title;
			}

			if (updatedCampaign.description !== undefined) {
				campaign.description = updatedCampaign.description;
			}

			if (updatedCampaign.conversionPercentage !== undefined) {
				campaign.conversionPercentage = updatedCampaign.conversionPercentage;
			}

			if (updatedCampaign.createdTimestamp !== undefined) {
				campaign.createdTimestamp = updatedCampaign.createdTimestamp;
			}

			campaign.updatedTimestamp = new Date().toISOString();

			result = campaign;

			break;
		}
	}

	// Save campaigns to disk.
	fs.writeFileSync(path.join(__dirname, '..', 'data', 'campaigns.json'), JSON.stringify(data));

	return result;
}

function getQueryParamTypes(params) {
	var result = null;

	if (params.queryParamName === 'filter') {
		result = JSON.parse(fs.readFileSync(path.join(__dirname, '..', 'data', 'filter-types.json')));
	} else if (params.queryParamName === 'sort') {
		result = JSON.parse(fs.readFileSync(path.join(__dirname, '..', 'data', 'sort-types.json')));
	}

	return result;
}

function getCampaign(params) {
	var result = null;
	return result;
}
