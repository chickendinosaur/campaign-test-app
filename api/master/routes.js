'use strict';

module.exports = function (router) {
	const campaignHandlers = require('./handlers/campaigns');

	// Routes.
	router.get('/api/v1/campaigns', campaignHandlers.getCampaigns);
	router.get('/api/v1/campaigns/:id', campaignHandlers.getCampaign);
	router.put('/api/v1/campaigns/:id', campaignHandlers.updateCampaign);
	router.post('/api/v1/campaigns/:id', campaignHandlers.createCampaign);
	router.delete('/api/v1/campaigns/:id', campaignHandlers.deleteCampaign);
	router.get('/api/v1/campaigns/query-param-types/:queryParamName', campaignHandlers.getQueryParamTypes);
};
