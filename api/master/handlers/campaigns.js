'use strict';

const repo = require('../repositories/campaigns');

module.exports = {
	getCampaign,
	getCampaigns,
	updateCampaign,
	createCampaign,
	deleteCampaign,
	getQueryParamTypes
};

function getCampaign(req, res) {}

function getCampaigns(req, res) {
	repo.getCampaigns(req.query)
		.then(function (value) {
			res.json({
				data: {
					campaigns: value
				}
			});
		});
}

function createCampaign(req, res) {
	var data = repo.createCampaign(req.params, req.query, req.body);

	res.status(200).json({
		data: {
			campaign: data
		},
		messages: data === null ?
			'Campaign to be copied was not found.' : 'A campagin copy has been created.'
	});
}

function deleteCampaign(req, res) {
	var data = repo.deleteCampaign(req.params);

	res.status(200).json({
		data: {
			campaign: data
		},
		messages: data === null ?
			'Campagin to delete does not exist.' : 'Campaign deleted successfully.'
	});
}

function updateCampaign(req, res) {
	console.log(req.body);
	var data = repo.updateCampaign(req.params, req.body);

	res.status(200).json({
		data: {
			campaign: data
		},
		messages: data === null ?
			'Campagin to edit does not exist.' : 'Campaign updated successfully.'
	});
}

function getQueryParamTypes(req, res) {
	var data = repo.getQueryParamTypes(req.params);

	res.status(200).json(data);
}
