This is a master api service that imports other api routes to aggregate as
a single service. The idea here would be to always run this until there is a needed to separate out the other api's into individual running services.

Reasons for separation:
- performance - (one of the api routes is overloading the service)
- data availability (if the master service goes down all data api's
are now unavailable)
